
public class Tester {
    public static void main(String[] args) {
        MyBST<Integer,Integer> tester = new MyBST<Integer,Integer>();
        tester.insert(5,0);
        tester.insert(80,0);
        tester.insert(4,0);
        tester.insert(10,0);
        tester.remove(4);
        System.out.println(tester.preOrder().toString());
    }
}