//Michael Ashmead (mashmea1)
import java.util.ArrayList;
import java.util.List;

public class MySplayTree<K extends Comparable<? super K>, V> implements BinarySearchTree<K,V> {
    /**
     * Current number of nodes in the tree.
     */
    private int size;
    
    /**
     * Top node of the tree.
     */
    private Node root;
    
    /**
     * Constructor method.
     */
    public MySplayTree() {
        this.root  = null;
        this.size = 0;
    }
    
    /**
     * @author Michael Ashmead
     */
    private class Node {
        
        /**
         * nkey.
         */
        private K nkey = null;
        
        /**
         * nval.
         */
        private V nval = null;
        
        /**
         * nleft.
         */
        private Node nleft = null;
        
        /**
         * nright.
         */
        private Node nright = null;
    }

    /**
     * @param theKey theKey
     * @param theValue theValue
     * @return boolean boolean
     */
    public boolean insert(K theKey, V theValue) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = theValue;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            this.root = this.splay(this.root, n);
            return false;
        } else {
            this.root = this.insertHelper(this.root, n);
            this.root = this.splay(this.root, n);
            this.size++;
            return true;
        }
    }
    
    /**
     * @param root2 root2
     * @param n n
     * @return Node Node
     */
    private Node splay(Node root2, Node n) {
        if (n.nkey.compareTo(root2.nkey) < 0) {
            if (n.nkey.compareTo(root2.nleft.nkey) < 0) {
                root2.nleft.nleft = this.splay(root2.nleft.nleft, n);
                root2 = this.rotateRight(root2);
            } else if (n.nkey.compareTo(root2.nleft.nkey) > 0) {
                root2.nleft.nright = this.splay(root2.nleft.nright, n);
                if (root2.nleft.nright != null) {
                    root2.nleft = this.rotateLeft(root2.nleft);
                }
            }
            return this.rotateRight(root2);
        }
        else if (n.nkey.compareTo(root2.nkey) > 0) {
            if (n.nkey.compareTo(root2.nright.nkey) < 0) {
                root2.nright.nleft  = this.splay(root2.nright.nleft, n);
                if (root2.nright.nleft != null) {
                    root2.nright = this.rotateRight(root2.nright);
                }
            } else if (n.nkey.compareTo(root2.nright.nkey) > 0) {
                //something not working correctly here?
                root2.nright.nright = this.splay(root2.nright.nright, n);
                root2 = this.rotateLeft(root2);
            }
            return this.rotateLeft(root2);
        }
        else {
            return root2;
        }
    }
    
    /**
     * @param root2 root2
     * @return Node Node
     */
    private Node rotateLeft(Node root2) {
        Node temp = root2.nright;
        root2.nright = temp.nleft;
        temp.nleft = root2;
        return temp;
    }

    /**
     * @param root2 root2
     * @return Node Node
     */
    private Node rotateRight(Node root2) {
        Node temp = root2.nleft;
        root2.nleft = temp.nright;
        temp.nright = root2;
        return temp;
    }
    
    /**
     * @param root2 root2
     * @param n n
     * @return boolean boolean
     */
    private boolean containsKey(Node root2, Node n) {
        if (root2 == null) {
            return false;
        }
        if (n.nkey.compareTo(root2.nkey) == 0) {
            return true;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            return this.containsKey(root2.nleft, n);
        } else {
            return this.containsKey(root2.nright, n);
        }
    }
    
    /**
     * @param root2 root2
     * @param n n
     * @return Node Node
     */
    private Node insertHelper(Node root2, Node n) {
        if (root2 == null) {
            return n;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            root2.nleft = this.insertHelper(root2.nleft, n);
        } else {
            root2.nright = this.insertHelper(root2.nright, n);
        }
        return root2;
    }

    /**
     * @param theKey theKey
     * @return V V
     */
    public V getValueFromKey(K theKey) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = null;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            this.root = this.splay(this.root, n);
            return this.getValueFromKeyHelper(this.root, n).nval;
        } else {
            //get nearest node and call splay on that node
            Node nearest = this.getNearestNode(this.root, n);
            this.root = this.splay(this.root, nearest);
            return null;
        }
    }
    
    /**
     * @param root2 root2
     * @param n n
     * @return Node node
     */
    private Node getNearestNode(Node root2, Node n) {
        if (n.nkey.compareTo(root2.nkey) < 0) {
            if (root2.nleft == null) {
                return root2;
            } else {
                return this.getNearestNode(root2.nleft, n);
            }
        } else {
            if (root2.nright == null) {
                return root2;
            } else {
                return this.getNearestNode(root2.nright, n);
            }
        }
    }
    /**
     * @param root2 root of tree
     * @param n Node to get value from
     * @return Node node
     */
    private Node getValueFromKeyHelper(Node root2, Node n) {
        if (n.nkey.compareTo(root2.nkey) == 0) {
            return root2;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            return this.getValueFromKeyHelper(root2.nleft, n);
        } else {
            return this.getValueFromKeyHelper(root2.nright, n);
        }
    }

    /**
     * @param theKey Key
     * @param theNewVal value to update with
     * @return boolean if successful or not
     */
    public boolean update(K theKey, V theNewVal) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = theNewVal;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            Node temp = this.getValueFromKeyHelper(this.root, n);
            temp.nval = theNewVal;
            this.root = this.splay(this.root, n);
            return true;
        } else {
            //get nearest node and call splay on that node
            Node nearest = this.getNearestNode(this.root, n);
            this.root = this.splay(this.root, nearest);
            return false;
        }
    }

    /**
     * @param theKey key
     * @return V - Value
     */
    public V remove(K theKey) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = null;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            n.nval = this.getValueFromKey(theKey); //causes n to splay to top
            if (this.root.nleft != null) {
                Node largest = this.largestInLeftSubtree(this.root.nleft);
                Node temp = this.root.nright;
                this.root = this.splay(this.root, largest);
                this.root.nright = temp;
            } else {
                this.root = this.root.nright;
            }
            this.size--;
            return n.nval;
        } else {
            //get nearest node and call splay on that node
            Node nearest = this.getNearestNode(this.root, n);
            this.root = this.splay(this.root, nearest);
            return null;
        }
    }
    
    /**
     * @param root2 the root of the subtree to be examined.
     * @return Node that is largest in left subtree.
     */
    private Node largestInLeftSubtree(Node root2) {
        if (root2.nright != null) {
            return this.largestInLeftSubtree(root2.nright);
        } else {
            return root2;
        }
    }
    /**
     * @return int that is the number of nodes in the tree.
     */
    public int size() {
        return this.size;
    }

    /**
     * @return boolean - Does the tree have any nodes or not?
     */
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    /**
     * @return List<K> of the tree's elements in pre-order.
     */
    public List<K> preOrder() {
        return this.preOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return List<K> of the tree's elements in pre-order.
     */
    private List<K> preOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.add(n.nkey);
            list.addAll(this.preOrderHelper(n.nleft));
            list.addAll(this.preOrderHelper(n.nright));
        }
        return list;
    }

    /**
     * @return List<K> of the tree's elements in order.
     */
    public List<K> inOrder() {
        return this.inOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return List<K> of the tree's elements in order.
     */
    private List<K> inOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.addAll(this.inOrderHelper(n.nleft));
            list.add(n.nkey);
            list.addAll(this.inOrderHelper(n.nright));
        }
        return list;
    }
    
    /**
     * @return List<K> of the tree's elements in post order.
     */
    public List<K> postOrder() {
        return this.postOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return List<K> of the tree's elements in post order.
     */
    private List<K> postOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.addAll(this.postOrderHelper(n.nleft));
            list.addAll(this.postOrderHelper(n.nright));
            list.add(n.nkey);
        }
        return list;
    }
}