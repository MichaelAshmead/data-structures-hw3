//Michael Ashmead (mashmea1)
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class BookIndexCreator {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        MyBST<String, String> tree = new MyBST<String, String>();

        String filename = args[0];

        File file = new File(filename);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = br.readLine();
        while (line != null && line.length() > 0) {
            //get key values
            String key = line.substring(2 + 2 + 1);
            if (key.indexOf("|") == -1) {
                key = key.substring(0, key.indexOf("}"));
            } else {
                key = key.substring(0, key.indexOf("|"));
            }
            //get value values
            String value = line.substring(2 + 2 + 1);
            value = value.substring(value.indexOf("{") + 1, value.length() - 1);
            //store key values into tree
            //if tree already has key, modify value's value
            if (tree.getValueFromKey(key) != null) {
                if (line.contains(")")) {
                    //closing sequence of pages
                    //get opening sequence page number
                    String open;
                    if (tree.getValueFromKey(key).contains(",")) {
                        String temp = tree.getValueFromKey(key);
                        int indexOf = temp.indexOf("-") - 1;
                        open = temp.substring(temp.lastIndexOf(","), indexOf);
                    } else {
                        String temp = tree.getValueFromKey(key);
                        open = temp.substring(0, temp.indexOf("-"));
                    }
                    //if closing page doesn't equal opening page
                    if (open.compareTo(value) != 0) {
                        String temp = tree.getValueFromKey(key);
                        tree.update(key, temp.concat(value));
                    } else {
                        String temp = tree.getValueFromKey(key);
                        tree.update(key, temp.substring(0, temp.indexOf("-")));
                    }
                } else if (line.contains("(")) {
                    //opening sequence of pages
                    String temp = tree.getValueFromKey(key);
                    tree.update(key, temp.concat("," + value + "-"));
                } else {
                    //a single page
                    String temp = tree.getValueFromKey(key);
                    tree.update(key, temp.concat("," + value));
                }
            } else { //otherwise go ahead and insert new key with value
                if (line.contains("(")) {
                    //opening sequence of pages
                    tree.insert(key, value + "-");
                } else {
                    //a single page
                    tree.insert(key, value);
                }
            }
            line = br.readLine();
        }
        br.close();
        //print out index from tree
        PrintWriter writer = new PrintWriter(filename + ".index", "UTF-8");
        List<String> list = tree.inOrder();
        for (int x = 0; x < list.size(); x++) {
            String temp = list.get(x);
            int count = temp.length() - temp.replace("!", "").length();
            for (int y = 0; y < count; y++) {
                writer.print("    ");
            }
            if (list.get(x).contains("!")) {
                writer.print(temp.substring(temp.lastIndexOf("!") + 1) + ": ");
            } else {
                writer.print(list.get(x) + ": ");
            }
            writer.println(tree.getValueFromKey(list.get(x)));
        }
        writer.close();
    }
}