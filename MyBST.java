//Michael Ashmead (mashmea1)
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michael Ashmead
 * @param <K>
 * @param <V>
 */
public class MyBST<K extends Comparable<? super K>, V> implements BinarySearchTree<K,V> {
    
    /**
     * Current number of nodes in the tree.
     */
    private int size;
    
    /**
     * Top node of the tree.
     */
    private Node root;
    
    /**
     * Constructor method.
     */
    public MyBST() {
        this.root  = null;
        this.size = 0;
    }
    
    /**
     * @author Michael Ashmead
     * Node class.
     */
    private class Node {
        /**
         * Key of node.
         */
        private K nkey = null;

        /**
         * Value of node.
         */
        private V nval = null;
        /**
         * Left child node.
         */
        private Node nleft = null;
        /**
         * Right child node.
         */
        private Node nright = null;
    }
    
    /**
     * @param theKey we want to insert.
     * @param theValue we want to insert with theKey.
     * @return boolean - Represent if there are no repeats
     * and the insert is successful.
     */
    public boolean insert(K theKey, V theValue) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = theValue;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            return false;
        } else {
            this.root = this.insertHelper(this.root, n);
            this.size++;
            return true;
        }
    }
    
    /**
     * @param root2 - Current root under inspection.
     * @param n - The Node about to be inserted.
     * @return Node returned.
     */
    private Node insertHelper(Node root2, Node n) {
        if (root2 == null) {
            return n;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            root2.nleft = this.insertHelper(root2.nleft, n);
        } else {
            root2.nright = this.insertHelper(root2.nright, n);
        }
        return root2;
    }

    /**
     * @param root2 current root under inspection.
     * @param n the Node we're searching for.
     * @return boolean that represents if the Node exists.
     */
    private boolean containsKey(Node root2, Node n) {
        if (root2 == null) {
            return false;
        }
        if (n.nkey.compareTo(root2.nkey) == 0) {
            return true;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            return this.containsKey(root2.nleft, n);
        } else {
            return this.containsKey(root2.nright, n);
        }
    }

    /**
     * @param theKey that we want to get the value of.
     * @return V the value.
     */
    public V getValueFromKey(K theKey) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = null;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            return this.getValueFromKeyHelper(this.root, n).nval;
        } else {
            return null;
        }
    }
    
    /**
     * @param root2 that is the root of the current tree under inspection.
     * @param n the Node (with theKey) that we are searching for.
     * @return Node that is the root of the current tree under inspection.
     */
    private Node getValueFromKeyHelper(Node root2, Node n) {
        if (n.nkey.compareTo(root2.nkey) == 0) {
            return root2;
        } else if (n.nkey.compareTo(root2.nkey) < 0) {
            return this.getValueFromKeyHelper(root2.nleft, n);
        } else {
            return this.getValueFromKeyHelper(root2.nright, n);
        }
    }

    /**
     * @param theKey that we want to find the Node for
     * @param theNewVal to be update the Node with theKey with
     * @return boolean that says if a Node with theKey was found or not
     */
    public boolean update(K theKey, V theNewVal) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = theNewVal;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            Node temp = this.getValueFromKeyHelper(this.root, n);
            temp.nval = theNewVal;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Removes Node from tree.
     * @param theKey - We want to find a Node with theKey to remove.
     * @return V - The value of the Node we remove (null if Node is not found).
     */
    public V remove(K theKey) {
        Node n = new Node();
        n.nkey = theKey;
        n.nval = null;
        n.nleft = null;
        n.nright = null;
        if (this.containsKey(this.root, n)) {
            n.nval = this.getValueFromKey(n.nkey);
            this.root = this.removeHelper(this.root, n);
            this.size--;
            return n.nval;
        } else {
            return null;
        }
    }
    
    /**
     * @param root2 is the root of current tree.
     * @param n is the node we are looking for to remove.
     * @return Node that is root of current tree.
     */
    private Node removeHelper(Node root2, Node n) {
        if (n.nkey.compareTo(root2.nkey) < 0) {
            root2.nleft = this.removeHelper(root2.nleft, n);
        } else if (n.nkey.compareTo(root2.nkey)  > 0) {
            root2.nright = this.removeHelper(root2.nright, n);
        } else { //found the Node we need to remove (root2.nkey = n.nkey)
            if (root2.nleft == null) {
                return root2.nright;
            } else if (root2.nright == null) {
                return root2.nleft;
            } else {
                //find minimum in right subtree of root and return that
                //now delete it
                Node min = this.getMinNode(root2.nright);
                root2.nright = this.removeHelper(root2.nright, min);
                return min;
            }
        }
        return root2;
    }
    
    /**
     * @param root2 - Current root of tree under inspection.
     * @return Node that has the minimum key.
     */
    private Node getMinNode(Node root2) {
        if (root2.nleft != null) {
            return this.getMinNode(root2.nleft);
        } else {
            return root2;
        }
    }

    /**
     * @return int that represents the number of nodes in the tree.
     */
    public int size() {
        return this.size;
    }

    /**
     * @return boolean - Represents if there are any nodes in the tree.
     */
    public boolean isEmpty() {
        if (this.size == 0) {
            return true;
        }
        return false;
    }

    /**
     * @return List<K> a list of preordered elements from the tree.
     */
    public List<K> preOrder() {
        return this.preOrderHelper(this.root);
    }
    
    /**
     * @param n the root of the whole tree
     * @return List<K> a list of preordered elements.
     */
    private List<K> preOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.add(n.nkey);
            list.addAll(this.preOrderHelper(n.nleft));
            list.addAll(this.preOrderHelper(n.nright));
        }
        return list;
    }

    /**
     * @return List<K> of elements in order.
     */
    public List<K> inOrder() {
        return this.inOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return List<K> of elements in order.
     */
    private List<K> inOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.addAll(this.inOrderHelper(n.nleft));
            list.add(n.nkey);
            list.addAll(this.inOrderHelper(n.nright));
        }
        return list;
    }

    /**
     * @return List<K> of the elements in post order.
     */
    public List<K> postOrder() {
        return this.postOrderHelper(this.root);
    }
    
    /**
     * @param n - The root of the whole tree.
     * @return List<K> of the elements in post order.
     */
    private List<K> postOrderHelper(Node n) {
        ArrayList<K> list = new ArrayList<K>();
        if (n != null) {
            list.addAll(this.postOrderHelper(n.nleft));
            list.addAll(this.postOrderHelper(n.nright));
            list.add(n.nkey);
        }
        return list;
    }
}